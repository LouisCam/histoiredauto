import unittest
import csv

from hdauto import date
from hdauto import main          #ne pourra pas fonctionner tant que l'erreur dans hdauto persistera mais je ne vois pas comment l'enlever
from hdauto import nom_colonne

class TestCSVAuto(unittest.TestCase):        
    def test_date(self):
        self.assertEqual(date("2012-12-30"),"2012-30-12") 

    def test_nom_colonne(self)    
        self.assertEqual(type(nom_colonne(file)),list)

    def test_main(self):
        file = "auto.csv"
        self.assertEqual(main("azerty.csv"),"ERREUR : Le fichier demande n'existe pas")

